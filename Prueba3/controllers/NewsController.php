<?php

namespace app\controllers;

use app\models\Sections;
use app\models\News;
use app\models\Upload;
use yii\web\Controller;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use Yii;
use app\models\form;
use yii\web\UploadedFile;

class NewsController extends Controller {

    public function actionForm() {

        $model = new News();
    
        if ($model->load(Yii::$app->request->post())) {
            $prueba = UploadedFile::getInstance($model, 'image');
            $prueba->saveAs('img/' . $prueba->name);
            $model->image = $prueba->name;
            if (!$model->save()) {

                return $this->render('form', ['model' => $model,
                            'sections' => $this->getSectionsOnArray()]);
            } else {

                $this->redirect(['news/detalle/'.$model->slug]);
            }
        } else {

            return $this->render('form', ['model' => $model,
                        'sections' => $this->getSectionsOnArray()]);
        }
    }
    
    
    public function actionDetalle($slug){
         
        $noticia= News::findOne(['slug'=>$slug]);
        $notice=$noticia->slug;
        print_r($notice);  
        return $this->render('detalle',['noticia'=>$noticia]);
       
        
    }

    private function findModelBySlug($slug) {
        if (($model = News::find()->where(['slug'=>$slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException();
        }
    }

    public function actionCreate() {
        $model = new News();
        $model->load(Yii::$app->request->post());
        $model->save();
        return $this->render('form', ['model' => $model]);
    }

    public function actionIndex() {
        $model= new News();
        $DataProvider = new ActiveDataProvider([
            'query' => News::find(),
            'pagination' => ['pagesize' => 10,],]);
        return $this->render('index', ['DataProvider' => $DataProvider,'model'=>$model]);
    }

    private function getSectionsOnArray() {

        return ArrayHelper::map(Sections::find()->all(), 'id', 'name');
    }

}
