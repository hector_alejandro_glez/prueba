<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;

class News extends \yii\db\ActiveRecord {

    public $file;

    /**
     * @property integer $id
     * @property string $title
     * @property string $body
     * @property integer $sections_id
     * @property integer $created_at
     * @property integer $updated_at
     * @property string  $image
     * @property string  $slug
     * @property Sections $sections
     */
    public static function tableName() {

        return 'news';
    }

    public function rules() {


        return
                    [
                        [['title', 'body', 'sections_id', 'image'], 'required'],
        ];
    }

    public function behaviors() {
        return [
                [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ], [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ],
        ];
    }

    public function upload() {
        if ($this->validate()) {
            $this->file->saveAs('img/' . $this->foto->baseName . '.' . $this->file->extension);

            return true;
        } else {
            return false;
        }
    }

    public function attributeLabels() {

        return
                    [
                    'id' => 'Identificador',
                    'title' => 'Titulo',
                    'body' => 'Cuerpo',
                    'sections_id' => 'Seccion',
                    'created_at' => 'Fecha_Creacion',
                    'updated_at' => 'Fecha_Actualizado',
                    'file' => 'image',
        ];
    }

    public function getSections() {



        return $this->hasMany(Sections::ClassName(), ['id' => 'sections_id']);
    }

}
