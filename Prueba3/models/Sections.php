<?php
 
namespace app\models;
use yii\db\ActiveRecord;
use yii;


class Sections extends ActiveRecord

{

    
    public static function tableName() 
    {
        
    return 'sections';
        
    }
    
    public function rules()
            
    { 
        
    return
        
    [
    
     [['name'],'string','max'=>255],'required',
     [['status'],'integer','required'],
     [['created_at'],'integer'],
     [['updated_at'],'integer'],
        
        
    ];
        
    }
    
    
    public function attributeLabels()
    
            
    {
        
    return
    [
    
     'id'=>'Identificador',
     'name'=>'Nombre',
     'status'=>'Status',
     'created_at'=>'Fecha_Creacion',
     'updated_at'=>'Fecha_Modificacion'
        
    ];    
        
    }        
    
    
    
    
    public function getNews()
     
    {
        
        return $this->hasMany(News::className(),['sections_id'=>'id']);
        
        
    }
    
    
    
    
    

    }
    
    
    
    
    
    
