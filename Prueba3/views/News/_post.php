<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

?>
<div class="post">
    
    <h2><?= Html::encode($model->id) ?></h2>
    <?= HtmlPurifier::process($model->title) ?>
    <br>    
    <?= HtmlPurifier::process($model->body) ?>    
    <?= Html::img('img/'.$model->image);?> 
   
</div>