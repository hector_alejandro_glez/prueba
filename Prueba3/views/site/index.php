<?php

use yii\grid\GridView;
use yii\widgets\ListView;
/* @var $this yii\web\View */
$model = new \app\models\Products;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Sistema</h1>

     

        
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-2">
               
             <h2>Heading</h2>

               </div>
    <div class="col-sm-4">
      <p class="text-center"><p class="bg-info" >

          
      <?php     echo ListView::widget([
    'dataProvider' => $dataProvider,
     'itemView' => '_post',
]); ?>
      
    </div>
    
                
            </div>
            <div class="col-lg-10">
              
            <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'description:ntext',
            'category.name',
            'price',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?> 
            </div>
           
        </div>

    </div>
</div>
