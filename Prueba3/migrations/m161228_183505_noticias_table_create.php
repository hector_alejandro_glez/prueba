<?php

use yii\db\Migration;

class m161228_183505_noticias_table_create extends Migration
{
    public function up()
    {
           $this->createTable('news',[
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'body' => $this->text()->notNull(),
            'sections_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ],'engine innodb'
                );
           
           
           
           $this->createIndex(
            'idx-news-sections_id',
            'news',
            'sections_id'
        );

        $this->addForeignKey(
            'fk-news-sections_id',
            'news',
            'sections_id',
            'sections',
            'id',
            'CASCADE'
        );

           

    }

    public function down()
    {
        echo "m161228_183505_noticias_table_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
